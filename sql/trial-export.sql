
# db charge_description by court
select charge_description, court_type, count(1)
from import_get_charges
group by charge_description, court_type
order by charge_description, court_type
;


# db arrest_codes too
select charge_description, court_type, arrest_code, count(1)
from import_get_charges
group by charge_description, court_type, arrest_code
order by charge_description, court_type, arrest_code
;


select * from import_get_charges limit 10;

select * from import_get_charges where charge_description  = 'CASS CO. MICHIGAN'


;




select * from import_get_charges;
select * from import_get_inmate;
select * from import_get_inmates;

# by inmate
# Name
# Race
# Ethnicity
# Gender
# Age
# Inmate Classification 


# Export 5/18
select 
i.arrest_no
, concat(i.first_name, ' ', i.last_name) as name
, i.current_age
, i.race
, i.sex

, i.arrest_date, i.booking_date, i.date_released, i.sched_release
, i.inmate_classification
from import_get_inmate i
;


# Export  8/25

select  A.arrest_no, A.name, A.current_age, A.race, A.sex
, A.arrest_date, A.booking_date, A.date_released, A.sched_release
, A.inmate_classification

, count(A.charge_id) as charge_count
, group_concat(A.charge_description) as charge_descriptions
, group_concat(A.is_violent) as is_violent_mappings

, max(case A.is_violent
	when 'Definite' then 1
	else 0
	end) as  any_charge_violent

, max(case A.is_violent
	when 'Definite' then 1
	when 'None' then 0
	when 'Possible' then 1
	when 'Endangerment' then 1
	else 0
	end) as any_charge_broader_violent


, group_concat(A.charge_category ) as categories
, max(case when A.charge_category= 'hold' then 1 else 0 end) as is_hold
, max(case when A.charge_category = 'force' then 1 else 0 end) as is_force
, max(case when A.charge_category = 'Parole Violation' then 1 else 0 end) as is_parole_violation
, max(case when A.charge_category = 'Weapon' then 1 else 0 end) as is_weapon
, max(case when A.charge_category = 'Property' then 1 else 0 end) as is_property
, max(case when A.charge_category = 'Other' then 1 else 0 end) as is_other
, max(case when A.charge_category = 'Body Attachment' then 1 else 0 end) as is_body_attachment
, max(case when A.charge_category = 'Sex Crimes' then 1 else 0 end) as is_sex_crime
, max(case when A.charge_category = 'Drugs' then 1 else 0 end) as is_drug_crime
, max(case when A.charge_category = 'OWI/Traffic' then 1 else 0 end) as is_traffic
, max(case when A.charge_category = 'Resisting Law Enforcement/Detention' then 1 else 0 end) as is_resisting
, max(case when A.charge_category = 'Return Per Court Order' then 1 else 0 end) as is_court_order

from 
(
	select 
	i.arrest_no
	, concat(i.first_name, ' ', i.last_name) as name
	, i.current_age
	, i.race
	, i.sex
	, i.arrest_date, i.booking_date, i.date_released, i.sched_release
	, i.inmate_classification
	
	, c.charge_id
	, c.charge_description
	, is_violent(c.charge_description) as is_violent
	, charge_desc_to_category(c.charge_description) as charge_category
	
		
	from import_get_inmate i
	inner join import_get_charges c on i.arrest_no = c.arrest_no
) A
group by A.arrest_no
;






