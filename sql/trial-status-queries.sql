

select 'import_get_inmates' as t, concat('row count: ', count(1)) as facet from import_get_inmates 
union all
select 'import_get_inmate' as t, concat('row count: ', count(1)) as facet from import_get_inmate 
union all
select 'import_get_charges' as t, concat('row count: ', count(1)) as facet from import_get_charges 
union all
select 'import_get_charges' as t, concat('distinct arrest_nos: ', count(distinct arrest_no)) as facet from import_get_charges
;

select * from import_get_charges limit 10;


select arrest_code, count(1), group_concat(distinct charge_description)
from import_get_charges 
group by arrest_code
order by arrest_code asc
limit 100;

select charge_description, count(1)
from import_get_charges 
group by charge_description
order by count(1) desc
limit 100;

select *
from import_get_inmates
where arrest_no between 72455 and 72457

select *
from import_get_charges
where arrest_no = 72455


