
create table import_status
(
arrest_no int not null primary key

, get_inmate varchar(15)
, get_inmate_ts datetime

, get_cases varchar(15)
, get_cases_ts datetime

, get_charges varchar(15)
, get_charges_ts datetime
);


create table import_get_inmates
(
arrest_no int not null primary key
, update_ts datetime
, agency_name varchar(100) null
, jacket varchar(15)
, first_name varchar(150)
, middle_name varchar(150)
, last_name varchar(150)
, suffix varchar(150)
, original_booking_datetime varchar(100)
, final_release_datetime varchar(100)
);


create table import_get_inmate
(
arrest_no int not null primary key
, update_ts datetime
, first_name varchar(150)
, last_name varchar(150)
, middle_name varchar(150)
, suffix varchar(150)
, alias varchar(150)
, current_age varchar(10)
, booking_date varchar(50)
, sched_release varchar(50)
, date_released varchar(50)
, height varchar(10)
, weight varchar(15)
, hair_color varchar(25)
, eye_color varchar(5)
, race varchar(5)
, sex varchar(5)
, arresting_officer varchar(45)
, badge varchar(45)
, arresting_agency varchar(150)
, arrest_date varchar(15)
, address varchar(150)
, zip varchar(15)
, inmate_classification varchar(150)
, bond_agency varchar(150)
);

create table import_get_charges
(
charge_id int not null primary key
, update_ts datetime
, arrest_no int not null
, case_no varchar(150)
, crime_type varchar(150)
, counts int
, modifier varchar(150)
, control_number varchar(150)
, warrant_number varchar(150)
, arrest_code varchar(150)
, charge_description varchar(1000)
, bond_type varchar(150)
, bond_amount decimal(15,2)
, court_type varchar(150)
, court_time varchar(150)
, charge_status varchar(150)
, offense_date varchar(150)
, arrest_date varchar(150)
, arresting_agency varchar(150)

);
