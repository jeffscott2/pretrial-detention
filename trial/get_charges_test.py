
import unittest

import trial.get_charges as testee

class TestStringMethods(unittest.TestCase):

	def test_parser_heading_mapper(self):

		url_str = b'{"data":[{"ChargeId":132346,"CaseNo":"71D012003F5000079","CrimeType":"F","Counts":1,"Modifier":null,"ControlNumber":"","WarrantNumber":"","ArrestCode":"35-47-2-1","ChargeDescription":"POSSESS HANDGUN WITHOUT LICENSE","BondType":"No Bail","BondAmount":0.00,"CourtType":"SUPERIOR (judge not assigned new site arrest)","CourtTime":"Mar 30 2020  1:00PM","ChargeStatus":"Warrant Issued","OffenseDate":"","ArrestDate":"2020-03-25","ArrestingAgency":""},{"ChargeId":132347,"CaseNo":"71D012003F5000079","CrimeType":"M","Counts":1,"Modifier":null,"ControlNumber":"","WarrantNumber":"","ArrestCode":"35-48-4-11A","ChargeDescription":"Carrying a Handgun Without a License -","BondType":"Bond Types","BondAmount":0.00,"CourtType":"","CourtTime":null,"ChargeStatus":"Warrant Issued","OffenseDate":"","ArrestDate":"2020-03-25","ArrestingAgency":""}],"totalCount":2,"error":"","success":"true"}'

		res = testee.parse_data_str(url_str)
		self.assertIsNotNone(res)
		self.assertEqual(2, len(res))

	def test_recent_inmates_row_parser(self):
		parsed_input = [
			{'ChargeId': 132346, 'CaseNo': '71D012003F5000079', 'CrimeType': 'F', 'Counts': 1, 'Modifier': None, 'ControlNumber': '', 'WarrantNumber': '',
				'ArrestCode': '35-47-2-1', 'ChargeDescription': 'POSSESS HANDGUN WITHOUT LICENSE', 'BondType': 'No Bail', 'BondAmount': 0.0,
				'CourtType': 'SUPERIOR (judge not assigned new site arrest)', 'CourtTime': 'Mar 30 2020  1:00PM', 'ChargeStatus': 'Warrant Issued',
				'OffenseDate': '', 'ArrestDate': '2020-03-25', 'ArrestingAgency': ''},
			{'ChargeId': 132347, 'CaseNo': '71D012003F5000079', 'CrimeType': 'M', 'Counts': 1, 'Modifier': None, 'ControlNumber': '', 'WarrantNumber': '',
				'ArrestCode': '35-48-4-11A', 'ChargeDescription': 'Carrying a Handgun Without a License -', 'BondType': 'Bond Types', 'BondAmount': 0.0,
				'CourtType': '', 'CourtTime': None, 'ChargeStatus': 'Warrant Issued', 'OffenseDate': '', 'ArrestDate': '2020-03-25', 'ArrestingAgency': ''}]

		res = testee.parse_charges(123, parsed_input)
		self.assertEqual(2, len(res))
		c = res[0]


		self.assertIsNotNone(c)
		self.assertEqual(132346, c.charge_id)
		self.assertIsNotNone(c.update_ts)
