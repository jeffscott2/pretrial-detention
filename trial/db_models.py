
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool
from sqlalchemy.sql import text

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Numeric, ForeignKey



Base = declarative_base()


class ImportStatus(Base):
    __tablename__ = 'import_status'

    arrest_no = Column(Integer, primary_key=True)
    get_inmate = Column(String)
    get_inmate_ts = Column(DateTime)
    get_cases = Column(String)
    get_cases_ts = Column(DateTime)
    get_charges = Column(String)
    get_charges_ts = Column(DateTime)

class ImportGetInmates(Base):
    __tablename__ = 'import_get_inmates'

    arrest_no = Column(Integer, primary_key=True)
    update_ts = Column(DateTime)
    agency_name = Column(String)
    jacket = Column(String)
    first_name = Column(String)
    middle_name = Column(String)
    last_name = Column(String)
    suffix = Column(String)
    original_booking_datetime = Column(String)
    final_release_datetime = Column(String)

class ImportGetInmate(Base):
    __tablename__ = 'import_get_inmate'

    arrest_no = Column(Integer, primary_key=True)
    update_ts = Column(DateTime)
    first_name = Column(String)
    last_name = Column(String)
    middle_name = Column(String)
    suffix = Column(String)
    alias = Column(String)
    current_age = Column(String)
    booking_date = Column(String)
    sched_release = Column(String)
    date_released = Column(String)
    height = Column(String)
    weight = Column(String)
    hair_color = Column(String)
    eye_color = Column(String)
    race = Column(String)
    sex = Column(String)
    arresting_officer = Column(String)
    badge = Column(String)
    arresting_agency = Column(String)
    arrest_date = Column(String)
    address = Column(String)
    zip = Column(String)
    inmate_classification = Column(String)
    bond_agency = Column(String)

class ImportGetCharges(Base):
    __tablename__ = 'import_get_charges'

    charge_id = Column(Integer, primary_key=True)
    update_ts = Column(DateTime)
    arrest_no = Column(Integer)
    case_no = Column(String)
    crime_type = Column(String)
    counts = Column(Integer)
    modifier = Column(String)
    control_number = Column(String)
    warrant_number = Column(String)
    arrest_code = Column(String)
    charge_description = Column(String)
    bond_type = Column(String)
    bond_amount = Column(Numeric)
    court_type = Column(String)
    court_time = Column(String)
    charge_status = Column(String)
    offense_date = Column(String)
    arrest_date = Column(String)
    arresting_agency = Column(String)

