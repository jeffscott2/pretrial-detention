
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool
from sqlalchemy.sql import text

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Numeric, ForeignKey

from .db_models import ImportGetInmates, ImportGetInmate, ImportGetCharges

class DbHelper():
    def __init__(self):
        (e, s) = connect_via_sql_alchemy("root", "", "127.0.0.1", "trial_data")
        self.engine = e
        self.session = s

class Crud():
    def save_new(self,session, o):
        session.add(o)
        session.commit()

    def update(self, session, o):
        print(f"Merging import_get_inmates arrest_no = {o.arrest_no}")
        session.merge(o)
        session.commit()


class QueryImportGetInmates():
    def __init__(self, db):
        self.engine = db.engine
        self.session = db.session

    def query_by_arrest_nos(self, ids):
        ids_str = ','.join([str(id) for id in ids])
        sql = f"select * from import_get_inmates where arrest_no in ({ids_str})"
        rows = self.session.query(ImportGetInmates).from_statement(text(sql)).all()
        return rows


class QueryImportGetInmate():
    def __init__(self, db):
        self.engine = db.engine
        self.session = db.session

    def query_by_arrest_no(self, id):

        sql = f"select * from import_get_inmate where arrest_no = {id}"
        row = self.session.query(ImportGetInmate).from_statement(text(sql)).one_or_none()
        return row

    def query_missing_from_list(self):

        sql ="""
select l.arrest_no
from import_get_inmates l
left join import_get_inmate i
  on l.arrest_no = i.arrest_no
where i.arrest_no is null
order by l.arrest_no
        """
        with self.engine.connect() as con:
            rs = con.execute(sql)

        result = [row['arrest_no'] for row in rs]

        return result


class QueryImportGetCharges():
    def __init__(self, db):
        self.engine = db.engine
        self.session = db.session

    def query_by_charge_ids(self, ids):
        ids_str = ','.join([str(id) for id in ids])

        sql = f"select * from import_get_charges where charge_id in ({ids_str})"
        rows = self.session.query(ImportGetCharges).from_statement(text(sql)).all()
        return rows

    def query_missing_from_list(self):
        sql = """
select l.arrest_no
from import_get_inmates l
left join import_get_charges i
  on l.arrest_no = i.arrest_no
where i.arrest_no is null
order by l.arrest_no
        """
        with self.engine.connect() as con:
            rs = con.execute(sql)

        result = [row['arrest_no'] for row in rs]
        return result


def connect_via_sql_alchemy(user, password, host, dbname):

    #'mysql+pymysql://<username>:<password>@<host>/<dbname>[?<options>]'
    if password is None :
        connection_string = f"mysql+pymysql://{user}@{host}/{dbname}"
    else:
        connection_string = f"mysql+pymysql://{user}:{password}@{host}/{dbname}"
    engine = create_engine(connection_string, poolclass=NullPool)
    Session = sessionmaker(bind=engine)
    session = Session()
    return engine, session





# class DataScraper:
# 	def __init__(self):
# 		engine, session = connect_via_sql_alchemy("root", None, "127.0.0.1", "covid")
# 		self.engine = engine
# 		self.session = session
#
# 	def download_url(self, url, file):
# 		print(f"Downloading to {file}")
# 		url_content = requests.get(url)
# 		open(file, 'wb').write(url_content.content)
#
# 	def load_file_to_db(self, file, table):
# 		print(f"Loading to DB: {file}")
# 		rows = load_file(file, 2)
#
# 		insert_sql = text(f"""
# 		REPLACE INTO {table}(state, region, lat, lon, date, value)
# 		VALUES(:state, :country, :lat, :lon, :date, :value)
# 		""")
#
# 		with self.engine.connect() as con:
# 			for row in rows:
# 				con.execute(insert_sql, **row)
#
# 	def exec_sql(self, sql):
# 		print(f"Running SQL: {sql}")
# 		self.session.execute(text(sql))
# 		self.session.commit()
#
# 	def close(self):
# 		self.session.close()
# 		self.engine.dispose()




# ids="1,4,50"
# sql = f"""
# select *
# from film
# where film_id in ({ids})
# """
# result = session.query(Film).from_statement(text(sql)).all()
# for row in result:
#     # print(type(row))
#     print(row.display())
