
import argparse

from .get_inmates import get_inmates_recent, get_inmates_for_date_range
from .get_inmate import get_inmate, get_inmate_missing
from .get_charges import get_charges, get_charges_missing

def add_get_inmates_recent_parser(subparsers):
	p = subparsers.add_parser("get_inmates_recent")
	p.set_defaults(func=lambda args: get_inmates_recent(30))

def add_get_inmates_date_range_parser(subparsers):
	p = subparsers.add_parser("get_inmates_in_range")
	p.add_argument("start_dt",help="start_dt YYYY-MM-DD")
	p.add_argument("end_dt",help="end_dt YYYY-MM-DD")
	p.set_defaults(func=lambda args: get_inmates_for_date_range(args.start_dt, args.end_dt))



def add_get_inmate_parser(subparsers):
	p = subparsers.add_parser("get_inmate")
	p.add_argument("arrest_no",help="arrest_no")
	p.set_defaults(func=lambda args: get_inmate(args.arrest_no))

def add_get_inmate_parser(subparsers):
	p = subparsers.add_parser("get_inmate_missing")
	p.set_defaults(func=lambda args: get_inmate_missing())



def add_get_charges_parser(subparsers):
	p = subparsers.add_parser("get_charges")
	p.add_argument("arrest_no",help="arrest_no")
	p.set_defaults(func=lambda args: get_charges(args.arrest_no))

def add_get_charges_missing_parser(subparsers):
	p = subparsers.add_parser("get_charges_missing")
	p.set_defaults(func=lambda args: get_charges_missing())


def run():

	parser = argparse.ArgumentParser(description='Pretrial Detention Project.')
	subparsers = parser.add_subparsers()

	add_get_inmates_recent_parser(subparsers)
	add_get_inmates_date_range_parser(subparsers)
	add_get_inmate_parser(subparsers)
	add_get_charges_parser(subparsers)
	add_get_charges_missing_parser(subparsers)

	args = parser.parse_args()

	args.func(args)