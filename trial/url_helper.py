
import requests
import trial.url_helper as url_helper

def download_url_to_file(url, file):
	print(f"Downloading to {file}")
	url_content = requests.get(url)
	open(file, 'wb').write(url_content.content)


def get_url_content(url):
	url_content = requests.get(url)
	return url_content


def post_url_content(url, data):
	url_content = requests.post(url, data = data)
	return url_content


class JailTrackerSessionManager:
	def __init__(self):
		self.s = requests.session()
		self.cookies = None
		self.session_base_url = None

	def connect(self):
		base_url = "https://omsweb.public-safety-cloud.com/jtclientweb/"
		# I believe this is the request that injects our location into the session
		first_url = "https://omsweb.public-safety-cloud.com/jtclientweb/jailtracker/index/StJoseph_County_IN"

		r = self.s.get(first_url)
		session_id = self.extract_session_id(r.url)
		self.session_base_url = base_url + session_id + "/JailTracker"

		a = 3

	def extract_session_id(self, url):
		return url.split("/")[4]

	def get_recent_inmates(self, limit, page):
		url = f"{self.session_base_url}/GetInmates?_dc=1585012240249&start={page}&limit={limit}&sort=OriginalBookDateTime&dir=DESC&searchtype=all&agencyid=0"
		print(f"Connecting to: {url}")
		return url_helper.get_url_content(url).content

	def get_inmates_for_date_range(self, start_dt, end_dt, count, page_idx):
		start = page_idx * count
		url = f"{self.session_base_url}/GetInmates?_dc=1585420478310&start={start}&limit={count}&sort=LastName&dir=ASC&firstname=&lastname=&searchtype=all&dosearchdaterange=True&begindate={start_dt}&enddate={end_dt}&agencyid=0"
		print(f"Connecting to: {url}")
		return url_helper.get_url_content(url).content

	def get_inmate(self, arrest_no):
		url = f"{self.session_base_url}/GetInmate?_dc=1585010698272&arrestNo={arrest_no}"
		print(f"Connecting to: {url}")
		return url_helper.get_url_content(url).content

	def get_charges(self, arrest_no):
		url = f"{self.session_base_url}/GetCharges"
		data = { 'arrestNo': arrest_no}
		print(f"Posting to: {url}")
		return url_helper.post_url_content(url, data).content
