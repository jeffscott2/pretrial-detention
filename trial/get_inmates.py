
from .url_helper import get_url_content, JailTrackerSessionManager
import json
import datetime, time
from datetime import timezone
import trial.db_models as models
from trial.db_helper import DbHelper, QueryImportGetInmates, Crud


def get_inmates_recent(recent_count):
	print("In get_inmates_recent()")
	jt = JailTrackerSessionManager()
	jt.connect()
	data_str = jt.get_recent_inmates(recent_count, 0)
	rows = parse_data_str(data_str)
	new_objs = [parse_inmate_row(row) for row in rows]
	save_db_objs(new_objs)


def get_inmates_for_date_range(start_dt, end_dt):
	print(f"In get_inmates_for_date_range({start_dt},{end_dt})")
	jt = JailTrackerSessionManager()
	jt.connect()

	page_size = 250
	for page_idx in range(0, 1001):
		print(f"Downloading Page #{page_idx}")
		data_str = jt.get_inmates_for_date_range(start_dt, end_dt, page_size, page_idx)
		rows = parse_data_str(data_str)
		new_objs = [parse_inmate_row(row) for row in rows]
		print(f"Found {len(new_objs)} new objects")
		save_db_objs(new_objs)
		if len(new_objs) < page_size:
			break
		time.sleep(1)



def save_db_objs(new_objs):

	db = DbHelper()
	q_get_inmates = QueryImportGetInmates(db)
	crud = Crud()

	new_arrest_nos = [o.arrest_no for o in new_objs]
	db_objs = q_get_inmates.query_by_arrest_nos(new_arrest_nos)
	db_arrest_nos = [o.arrest_no for o in db_objs]

	objs_to_save = [o for o in new_objs if o.arrest_no not in db_arrest_nos]

	if len(objs_to_save) == 0:
		print("No new arrest_no's found")
	else:
		for o in objs_to_save:
			print(f"Saving import_get_inmates arrest_no = {o.arrest_no}")
			crud.save_new(db.session, o)


def parse_data_str(data_str):
	data = json.loads(data_str)['data']
	return data


def parse_inmate_row(row):
	res = models.ImportGetInmates()
	res.arrest_no = row['ArrestNo']
	res.update_ts = datetime.datetime.now(timezone.utc)
	res.agency_name = row['AgencyName']
	res.jacket = row['Jacket']
	res.first_name = row['FirstName']
	res.middle_name = row['MiddleName']
	res.last_name = row['LastName']
	res.suffix = row['Suffix']
	res.original_booking_datetime = row['OriginalBookDateTime']
	res.final_release_datetime = row['FinalReleaseDateTime']
	return res


