
import unittest

import trial.get_inmate as testee

class TestGetInmate(unittest.TestCase):

	def test_parser_heading_mapper(self):

		get_inmate_str = b'{"data":[{"Field":"Sched Release:","Value":""},{"Field":"First Name:","Value":"JULIEN"},{"Field":"M Name:","Value":""},{"Field":"Last Name:","Value":"TORNILLO"},{"Field":"Suffix:","Value":""},{"Field":"Alias:","Value":""},{"Field":"Age:","Value":"24"},{"Field":"Book Dt:","Value":"03/18/2020"},{"Field":"Date Released:","Value":"3/23/2020 1:21:14 PM"},{"Field":"Height:","Value":"5\\u0027 10\\""}],"totalCount":10,"error":"","success":"true"}'

		res = testee.parse_data_str(get_inmate_str)
		self.assertIsNotNone(res)
		self.assertEqual(10, len(res))

	def test_recent_inmates_row_parser(self):
		i = [{'Field': 'Sched Release:', 'Value': ''},
			 {'Field': 'First Name:', 'Value': 'JULIEN'},
			 {'Field': 'M Name:', 'Value': ''},
			 {'Field': 'Last Name:', 'Value': 'TORNILLO'},
			 {'Field': 'Suffix:', 'Value': ''},
			 {'Field': 'Alias:', 'Value': ''},
			 {'Field': 'Age:', 'Value': '24'},
			 {'Field': 'Book Dt:', 'Value': '03/18/2020'},
			 {'Field': 'Date Released:', 'Value': '3/23/2020 1:21:14 PM'},
			 {'Field': 'Height:', 'Value': '5\' 10"'}]

		res = testee.parse_inmate(123, i)

		self.assertIsNotNone(res)
		self.assertEqual(123, res.arrest_no)
		self.assertIsNotNone(res.update_ts)
		self.assertEqual('JULIEN', res.first_name)
		self.assertEqual('', res.suffix)
		self.assertEqual('03/18/2020', res.booking_date)
		self.assertEqual('', res.middle_name)
		self.assertIsNone(res.zip)
		self.assertEqual("5' 10\"", res.height)



