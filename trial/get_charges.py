
from .url_helper import get_url_content, JailTrackerSessionManager
import json
import datetime, time
from datetime import timezone
import trial.db_models as models
from trial.db_helper import DbHelper, QueryImportGetCharges, Crud

jt = JailTrackerSessionManager()
jt.connect()

db = DbHelper()
q = QueryImportGetCharges(db)
crud = Crud()


def now():
	return datetime.datetime.utcnow()


def get_charges_missing():
	print(f"In get_charges_missing()")

	arrest_nos = q.query_missing_from_list()
	for arrest_no in arrest_nos:
		t1 = now()
		get_charges(arrest_no)
		print(f"Downloaded & saved in: {(now() - t1).microseconds / 1000}ms")
		time.sleep(.25)


def get_charges(arrest_no):
	print(f"In get_charges({arrest_no})")

	data_str = jt.get_charges(arrest_no)
	rows = parse_data_str(data_str)
	if len(rows) == 0:
		print(f"Didn't find any charges for arrest_no: {arrest_no}")
		return
	new_objs = parse_charges(arrest_no, rows)


	new_charge_ids = [o.charge_id for o in new_objs]
	db_objs = q.query_by_charge_ids(new_charge_ids)
	db_charge_ids = [o.charge_id for o in db_objs]

	for new_obj in new_objs:
		print(f"Saving import_get_charges charge_id = {new_obj.charge_id}")
		if new_obj.charge_id in db_charge_ids:
			crud.update(db.session, new_obj)
		else:
			crud.save_new(db.session, new_obj)
	# objs_to_save = [o for o in new_objs if o.charge_id not in db_charge_ids]
	#
	# if len(objs_to_save) == 0:
	# 	print("No new charge_id's found")
	# else:
	# 	for o in objs_to_save:
	# 		print(f"Saving import_get_inmates arrest_no = {o.arrest_no}")
	# 		crud.save_new(db.session, o)
	a = 3


def parse_data_str(data_str):
	data = json.loads(data_str)['data']
	return data


def parse_charges(arrest_no, rows):
	return [parse_charge(arrest_no, row) for row in rows]


def parse_charge(arrest_no, row):
	field_map = {
		"ChargeId": 'charge_id',
		"CaseNo": 'case_no',
		"CrimeType": 'crime_type',
		"Counts": 'counts',
		"Modifier": 'modifier',
		"ControlNumber": 'control_number',
		"WarrantNumber": 'warrant_number',
		"ArrestCode": 'arrest_code',
		"ChargeDescription": 'charge_description',
		"BondType": 'bond_type',
		"BondAmount": 'bond_amount',
		"CourtType": 'court_type',
		"CourtTime": 'court_time',
		"ChargeStatus": 'charge_status',
		"OffenseDate": 'offense_date',
		"ArrestDate": 'arrest_date',
		"ArrestingAgency": 'arresting_agency',
	}

	res = models.ImportGetCharges()
	res.arrest_no = arrest_no
	res.update_ts = datetime.datetime.now(timezone.utc)
	for field,value in row.items():
		if field not in field_map:
			print(f"Unable to map Field: {field}")
			continue

		map_target = field_map[field]

		v = value.strip() if isinstance(value, str) else value
		setattr(res, map_target, v)

	return res


