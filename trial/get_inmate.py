
from .url_helper import get_url_content, JailTrackerSessionManager
import json, datetime, time
from datetime import timezone
import trial.db_models as models
from trial.db_helper import DbHelper, QueryImportGetInmate, Crud

db = DbHelper()
q = QueryImportGetInmate(db)
crud = Crud()

jt = JailTrackerSessionManager()
jt.connect()

def now():
	return datetime.datetime.utcnow()

def get_inmate_missing():
	print(f"In get_inmate_missing()")

	arrest_nos = q.query_missing_from_list()
	for arrest_no in arrest_nos:
		t1 = now()
		get_inmate(arrest_no)
		print (f"Downloaded & saved in: {(now() -t1).microseconds/1000}ms")
		time.sleep(.25)

def get_inmate(arrest_no):
	print(f"In get_inmate({arrest_no})")

	data_str = jt.get_inmate(arrest_no)
	data = parse_data_str(data_str)

	new_obj = parse_inmate(arrest_no, data)
	save_obj(arrest_no, new_obj)




def save_obj(arrest_no, new_obj):

	db_obj = q.query_by_arrest_no(arrest_no)

	if db_obj is None:
		crud.save_new(db.session, new_obj)
	else:
		crud.update(db.session, new_obj)


	# rows = parse_data_str(data_str)
	# new_objs = [parse_inmate_row(row) for row in rows]
	#
	# db = DbHelper()
	# get_inmates_crud = ImportGetInmatesCrud(db)
	#
	# new_arrest_nos = [o.arrest_no for o in new_objs]
	# db_objs = get_inmates_crud.query_by_arrest_nos(new_arrest_nos)
	# db_arrest_nos = [o.arrest_no for o in db_objs]
	#
	# objs_to_save = [o for o in new_objs if o.arrest_no not in db_arrest_nos]
	#
	# get_inmates_recent
	# if len(objs_to_save) == 0:
	# 	print("No new arrest_no's found")
	# else:
	# 	saved_objects = [get_inmates_crud.save_new(o) for o in objs_to_save]

	a = 3

def parse_data_str(data_str):
	data = json.loads(data_str)['data']
	return data


def parse_inmate(arrest_no, row):

	field_map = {
		"First Name:": 'first_name',
		"Middle Name:": 'middle_name',
		"M Name:": 'middle_name',
		"Last Name:": 'last_name',
		"Suffix:": 'suffix',
		"Alias:": 'alias',
		"Age:": 'current_age',
		"Current Age:": 'current_age',
		"Book Dt:": 'booking_date',
		"Booking Date:": 'booking_date',
		"Sched Release:": 'sched_release',
		"Date Released:": 'date_released',
		"Height:": 'height',
		"Weight:": 'weight',
		"Hair Color:": 'hair_color',
		"Eye Color:": 'eye_color',
		"Race:": 'race',
		"Sex:": 'sex',
		"Arresting Officer:": 'arresting_office',
		"Badge:": 'badge',
		"Arresting Agency:": 'arresting_agency',
		"Arrest Date:": 'arrest_date',
		"Address:": 'address',
		"Zip:": 'zip',
		"Inmate Classification:": 'inmate_classification',
		"Bond Agency:": 'bond_agency'
	}

	res = models.ImportGetInmate()
	res.arrest_no = arrest_no
	res.update_ts = datetime.datetime.now(timezone.utc)
	for kv in row:
		field = kv['Field']
		value = kv['Value']
		if field not in field_map:
			print(f"Unable to map Field: {field}")
			continue

		map_target = field_map[field]
		setattr(res, map_target, value.strip())

	return res


