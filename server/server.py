from flask import Flask, escape, request, Response, send_from_directory, jsonify
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}
							, r"/assets/*": {"origins": "*"}
							})


@app.route('/assets/<path:path>')
def assets(path):
    return send_from_directory('assets', path)


@app.route('/')
def hello():
    return "hello"

    # const
    # domContainer = document.querySelector('#like_button_container');
    # ReactDOM.render(e(LikeButton), domContainer);


@app.route('/api/import_status')
def import_status():
    return jsonify({"status": "ok"})
